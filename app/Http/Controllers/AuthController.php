<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Utils\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\AuthController\LoginRequest;
use App\Http\Requests\AuthController\RegisterRequest;
use App\Http\Requests\AuthController\ChangePasswordRequest;
use App\Http\Requests\AuthController\ForgotPasswordRequest;

class AuthController extends Controller
{
    /**
     * App\Utils\Response;
     */
    protected $response;

    /**
     * Constructor 
     * @param App\Utils\Response $response
     *
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * Register User Client
     * @param App\Http\Requests\AuthController\RegisterRequest  $request
     */
    public function register(RegisterRequest $request){
        try {
            $validated = $request->validated();

            $userEmail = User::where('email', $validated['email'])->first();

            if($userEmail) {
                return $this->response->setCode('MSW00006')
                ->setMessage('Địa chỉ Email đã được đăng ký bởi tài khoảng khác. ')
                ->getResponse(401);
            }

            $user = User::create([
                'name' => $validated['name'],
                'email' => $validated['email'],
                'password' => Hash::make($validated['password']),
                'birthDay' => $validated['birthDay'],
                'avatar' => $validated['avatar'],
                'phone' => $validated['phone'],
            ]);

            $token = $user->createToken('myApp')->plainTextToken;

            return $this->response
                    ->setCode('MSW00010')
                    ->setMessage('Successfully')
                    ->setData([
                        'name' => $user->name,
                        'email' => $user->email,
                        'birthDay' => $user->birthDay,
                        'avatar' => $user->avatar,
                        'phone' => $user->phone,
                        'token' => $token
                    ])
                    ->getResponse(200);
        } catch (\Exception $e) {

            \Log::error($e);
            return $this->response->setCode('MSW00006')
            ->setMessage('Lỗi hệ thống.')
            ->getResponse(500);

        }

    }

    /**
     * Login User Client
     * @param App\Http\Requests\AuthController\LoginRequest  $request
     */
    public function login(LoginRequest $request){

        try {
            
        $validated = $request->validated();

        $user = User::where('email', $validated['email'])->first();

        if(!$user){
            return $this->response->setCode('MSW00006')
                    ->setMessage('Địa chỉ Email không tồn tại. ')
                    ->getResponse(401);
        }

        if (!Hash::check($validated['password'], $user->password)) {
            return $this->response->setCode('MSW00006')
                    ->setMessage('Thông tin đăng nhập không chính xác.')
                    ->getResponse(401);
        }

        $token = $user->createToken('myApp')->plainTextToken;

        return $this->response
                ->setCode('MSW00010')
                ->setMessage('Successfully')
                ->setData([
                    'name' => $user->name,
                    'email' => $user->email,
                    'birthDay' => $user->birthDay,
                    'avatar' => $user->avatar,
                    'phone' => $user->phone,
                    'token' => $token
                ])
                ->getResponse(200);
        }catch (\Exception $e) {
            \Log::error($e);
            return $this->response->setCode('MSW00006')
            ->setMessage('Lỗi hệ thống.')
            ->getResponse(500);

        }
    }


     /**
     * Logout User Client
     * @param Illuminate\Http\Request  $request
     */
    public function logout(Request $request){
        try {
            auth()->user()->tokens()->delete();

                return  $this->response->setCode('MSW00006')
                    ->setMessage('Đăng suất thành công.')
                    ->getResponse(200);
        }catch (\Exception $e) {
            \Log::error($e);
            return $this->response->setCode('MSW00006')
            ->setMessage('Lỗi hệ thống.')
            ->getResponse(500);

        }
    }

    /**
     * Change Password User Client
     * @param App\Http\Requests\AuthController\ChangePasswordRequest  $request
     */

    public function changePassword(ChangePasswordRequest $request){
        try {
            $validated = $request->validated();

        $email = auth()->user()->email;

        $user = User::where('email', $email)->first();

        if (!Hash::check($validated['password_old'], $user->password)) {
            return $this->response->setCode('MSW00006')
                    ->setMessage('Mật khẩu củ không chính xác.')
                    ->getResponse(422);
        }

        if (Hash::check($validated['password_new'], $user->password)) {
            return $this->response->setCode('MSW00006')
            ->setMessage('Mật khẩu mới không được giống với mật khẩu củ. ')
            ->getResponse(422);
        }

        $user->update([
            'password' => Hash::make($validated['password_new'])
        ]);


        return $this->response->setCode('MSG2000')
            ->setMessage('Đổi mật khẩu thành công.')
            ->getResponse(200);
        } catch(\Exception $e) {
            \Log::error($e);
            return $this->response->setCode('MSW00006')
            ->setMessage('Lỗi hệ thống.')
            ->getResponse(500);

        }
    }

    /**
     * forgot Password userMaster
     * @param App\Http\Requests\AuthController\ForgotPasswordRequest $request;
     */
    public function forgotPassword(ForgotPasswordRequest $request){
        try {
        $validated = $request->validated();

        $user = User::where('email', $validated['email'])
                    ->where('name', $validated['name'])->first();

        if (!$user) {
            return $this->response->setCode('MSW00006')
            ->setMessage('Thông tin không chính xác. ')
            ->getResponse(401);
        }

        $user->update([
            'password' => Hash::make($validated['password'])
        ]);

        return $this->response->setCode('MSG2000')
        ->setMessage('Reset mật khẩu thành công. ')
        ->getResponse(200);
        } catch(\Exception $e) {
            \Log::error($e);
            return $this->response->setCode('MSW00006')
            ->setMessage('Lỗi hệ thống.')
            ->getResponse(500);

        }
    }
}
