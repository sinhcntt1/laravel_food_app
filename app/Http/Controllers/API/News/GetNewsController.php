<?php

namespace App\Http\Controllers\API\News;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Utils\Response;
use App\Models\News;
use Exception;

class GetNewsController extends Controller
{

     /**
     * App\Utils\Response;
     */
    protected $response;

    /**
     * Constructor 
     * @param App\Utils\Response $response
     *
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        try {
            $news = News::all();
            
            return $this->response
            ->setCode('MSW00010')
            ->setMessage('Successfully')
            ->setData($news)
            ->getResponse(200);

        } catch(Exception $e){
            \Log::error($e);
            return $this->response->setCode('MSW00006')
            ->setMessage('Lỗi hệ thống.')
            ->getResponse(500);
        }


    }
}
