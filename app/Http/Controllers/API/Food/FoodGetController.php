<?php

namespace App\Http\Controllers\API\Food;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Food;
use App\Utils\Response;
use Exception;

class FoodGetController extends Controller
{

     /**
     * App\Utils\Response;
     */
    protected $response;

    /**
     * Constructor 
     * @param App\Utils\Response $response
     *
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
       try {

        $paramCategory = $request->input('categoryId');

        if($paramCategory){
            $food = Food::where('category_id', $paramCategory)->get();
        } else {
            $food = Food::all();
        }

        $listFood = array();

        foreach ($food as $value) {
            $item = [
                "created_at" => $value->created_at,
                "updated_at" => $value->updated_at,
                "id" =>  $value->id,
                "categoryId" =>  $value->category_id,
                "title" => $value->title,
                "thumbnail" => $value->thumbnail,
                "description" => $value->description
            ];   
            array_push($listFood, $item);
        }

        return $this->response
        ->setCode('MSW00010')
        ->setMessage('Successfully')
        ->setData($listFood)
        ->getResponse(200);
           
       } catch (Exception $e) {
        \Log::error($e);
        return $this->response->setCode('MSW00006')
        ->setMessage('Lỗi hệ thống.')
        ->getResponse(500);
       }
    }
}
