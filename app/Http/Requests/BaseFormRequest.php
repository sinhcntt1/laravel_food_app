<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Utils\Response;

class BaseFormRequest extends FormRequest
{
    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Http\Exceptions\HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        \Log::warning(
            \Route::currentRouteAction() . ":\n" . json_encode($validator->errors()->toArray(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)
        );

        $response = new Response(null, 422, "không hợp lệ.");
        $response->setErrors($validator->errors()->toArray());
        throw new HttpResponseException($response->getResponse(422));
    }
}
