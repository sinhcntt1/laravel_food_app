<?php

namespace App\Http\Requests\AuthController;

use App\Http\Requests\BaseFormRequest;

class RegisterRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email'=> 'required|string|email',
            'phone' => 'required|string',
            'birthDay' => 'required|date',
            'password' => 'required|string|confirmed',
            'avatar' => 'required|string'
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => 'name',
            'email'=> 'email',
            'phone' => 'phone',
            'birthDay' => 'birthDay',
            'password' => 'password',
            'avatar' => 'avatar'
        ];
    }
}
