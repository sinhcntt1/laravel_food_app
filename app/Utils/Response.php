<?php

namespace App\Utils;

class Response
{

    private $code;
    private $message;
    private $data;
    private $errors;
    private $exception;


    /**
     * Create a new empty controller instance.
     */
    public function __construct()
    {
        $arguments = func_get_args();
        $numberOfArguments = func_num_args();

        if (method_exists($this, $function = '__construct' . $numberOfArguments)) {
            call_user_func_array(array($this, $function), $arguments);
        }
    }

    /**
     * Create a new controller instance.
     *
     * @param $data
     * @param $code
     * @param $message
     * @return void
     */
    public function __construct3($data, $code, $message)
    {
        $this->code = (string) $code;
        $this->message = $message;
        $this->data = $data;
    }

    /**
     * Set code value.
     *
     * @param $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = (string) $code;
        return $this;
    }

    /**
     * Set message value.
     *
     * @param $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Set data.
     *
     * @param $data
     * @return $this;
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Set errors.
     *
     * @param $errors
     * @return $this
     */
    public function setErrors($errors)
    {
        $data = [];
        foreach ($errors as $key => $value) {
            array_push($data, ["field" => $key, "message" => $value]);
        }
        $this->errors = $data;
        return $this;
    }

    /**
     * Set exception.
     *
     * @param $errors
     * @return $this
     */
    public function setException($code, $field, $message)
    {
        $data = null;
        if ($code) {
            $data['code'] = (string) $code;
        }
        if ($field) {
            $data['field'] = $field;
        }
        if ($message) {
            $data['message'] = $message;
        }
        if ($data) {
            $this->exception = [0 => $data];
        }
        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get message.
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Get data.
     *
     * @return object
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Get error.
     *
     * @return object
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Get exception.
     *
     * @return object
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * Get response.
     *
     * @param $httpStatus
     * @return
     */
    public function getResponse($httpStatus)
    {
        $data = [];
        
        if($httpStatus == 200) {
            $data['error'] = 0;
        } else if ($this->code) {
            $data['error'] = $this->code;
        }
        

        if ($this->data instanceof \Illuminate\Pagination\LengthAwarePaginator) {
            $data['total'] = $this->data->total();
            $data['per_page'] = $this->data->perPage();
            $data['current_page'] = $this->data->currentPage();
            $data['data'] = $this->data->items();
        } 

        if ($this->exception) {
            $data['error'] = $this->exception;
            if ($this->errors) {
                $data['error']['errors'] = $this->errors;
            }
        }

        if (!$this->exception && $this->errors) {
            $data['error'] = $this->errors;
        }
        $data['status'] = $httpStatus;
        $data['data'] = $this->data;

        if ($this->message) {
            $data['message'] = $this->message;
        }

        return response()->json($data, $httpStatus);
    }
}
