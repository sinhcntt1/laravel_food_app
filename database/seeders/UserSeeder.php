<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds = [
            [
               'name' => 'vansinh',
               'email' => 'example@gmail.com',
               'phone' => '0353062239',
               'password' =>  Hash::make('123456'),
               'avatar' => 'abc.png',
               'birthDay' => '20211111'
            ],
            [
                'name' => 'A',
                'email' => 'example12@gmail.com',
                'phone' => '0353062239',
                'password' =>  Hash::make('123456'),
                'avatar' => 'abc.png',
                'birthDay' => '20211111'
            ],
            [
                'name' => 'B',
                'email' => 'example123@gmail.com',
                'phone' => '0353062239',
                'password' =>  Hash::make('123456'),
                'avatar' => 'abc.png',
                'birthDay' => '20211111'
            ]
        ];

        $seeds = array_map(function ($seeds) {
            return array_merge($seeds, [
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }, $seeds);

        foreach (array_chunk($seeds, 1000) as $seeds) {
            User::query()->insert($seeds);
        }
    }
}
