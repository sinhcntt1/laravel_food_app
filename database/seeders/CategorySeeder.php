<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds = [
            [
               'title' => 'Món ngon từ tôm',
               'thumbnail' => 'food_CATEGORY_upload_at_1610810231965.jpg'
            ],
            [
                'title' => 'Món ngon từ cá',
                'thumbnail' => 'food_CATEGORY_upload_at_1610810538573.jpg'
            ],
            [
                'title' => 'Món ngon từ thịt bò',
                'thumbnail' => 'food_CATEGORY_upload_at_1609390920541.jpg'
            ],
            [
                'title' => 'Món ngon từ thịt heo',
                'thumbnail' => 'food_CATEGORY_upload_at_1609391023241.jpg'
            ],
            [
                'title' => 'Món ngon từ vịt',
                'thumbnail' => 'food_CATEGORY_upload_at_1609391090741.jpg'
            ],
            [
                'title' => 'Món ăn vặt',
                'thumbnail' => 'food_CATEGORY_upload_at_1609391282171.jpg'
            ],
            [
                'title' => 'Món ngon từ mực',
                'thumbnail' => 'food_CATEGORY_upload_at_1609391391653.jpg'
            ],
            [
                'title' => 'Món ngon từ thịt gà',
                'thumbnail' => 'food_CATEGORY_upload_at_1610163252545.jpg'
            ],
        ];

        $seeds = array_map(function ($seeds) {
            return array_merge($seeds, [
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }, $seeds);

        foreach (array_chunk($seeds, 1000) as $seeds) {
            Category::query()->insert($seeds);
        }
    }
}
