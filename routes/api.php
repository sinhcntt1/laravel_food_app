<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * Api register user client
 */
Route::post('/register', [\App\Http\Controllers\AuthController::class, 'register'])->name('register');

/**
 * Api login user client
 */
Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login'])->name('login');

/**
* Api forgotPassword user client
*/
Route::post('/forgotPassword', [\App\Http\Controllers\AuthController::class, 'forgotPassword'])->name('forgotPassword');

/**
 * Validate with Token
 */
Route::middleware('auth:sanctum')->group(function () {

    /**
     * Api logout user client
     */
    Route::post('/logout', [\App\Http\Controllers\AuthController::class, 'logout'])->name('logout');

    /**
     * Api changePassword user client
     */
    Route::post('/changePassword', [\App\Http\Controllers\AuthController::class, 'changePassword'])->name('changePassword');

    /**
     * Get Category
     */
    Route::get('/category', \App\Http\Controllers\API\Category\CategoryGetController::class)->name('getCategory');

    /**
     * get Food
     */
    Route::get('/food', \App\Http\Controllers\API\Food\FoodGetController::class)->name('getFood');

    /**
     * get all News
     */
    Route::get('/news', \App\Http\Controllers\API\News\GetNewsController::class)->name('getAllNews');

});
